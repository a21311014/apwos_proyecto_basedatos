<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\LogsController;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\AccesoriesController;
use App\Http\Controllers\LaptopsgamerController;
use App\Http\Controllers\PcgamersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', [AboutController::class, 'about'])->name('about'); 

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('users')->group(function(){

    Route::get('/', [UsersController::class, 'index'])->name('users');
    Route::get('/add', [UsersController::class, 'create'])->name('users-add');
    Route::get('/update', [UsersController::class, 'update'])->name('users-update');
    Route::get('/{id}', [UsersController::class, 'show'])->name('users-show');
});
Route::prefix('roles')->group(function(){

    Route::get('/', [RolesController::class, 'index'])->name('roles');
    Route::get('/add', [RolesController::class, 'create'])->name('roles-add');
    Route::get('/update', [RolesController::class, 'update'])->name('roles-update');
    Route::get('/{id}', [RolesController::class, 'show'])->name('roles-show');
});
Route::prefix('logs')->group(function(){

    Route::get('/', [LogsController::class, 'index'])->name('logs');
    Route::get('/add', [LogsController::class, 'create'])->name('logs-add');
    Route::get('/update', [LogsController::class, 'update'])->name('logs-update');
    Route::get('/{id}', [LogsController::class, 'show'])->name('logs-show');
});
Route::prefix('brands')->group(function(){

    Route::get('/', [BrandsController::class, 'index'])->name('brands');
    Route::get('/add', [BrandsController::class, 'create'])->name('brands-add');
    Route::post('/add', [BrandsController::class, 'create'])->name('brands-add');
    Route::get('/update', [BrandsController::class, 'update'])->name('brands-update');
    Route::get('/{id}', [BrandsController::class, 'show'])->name('brands-show');
});
Route::prefix('laptopsgamer')->group(function(){

    Route::get('/', [LaptopsgamerController::class, 'index'])->name('laptopsgamer');
    Route::get('/add', [LaptopsgamerController::class, 'create'])->name('laptopsgamer-add');
    Route::get('/update', [LaptopsgamerController::class, 'update'])->name('laptopsgamer-update');
    Route::get('/{id}', [LaptopsgamerController::class, 'show'])->name('laptopsgamer-show');
});
Route::prefix('pcgamers')->group(function(){

    Route::get('/', [PcgamersController::class, 'index'])->name('pcgamers');
    Route::get('/add', [PcgamersController::class, 'create'])->name('pcgamers-add');
    Route::get('/update', [PcgamersController::class, 'update'])->name('pcgamers-update');
    Route::get('/{id}', [PcgamersController::class, 'show'])->name('pcgamers-show');
});
Route::prefix('accesories')->group(function(){

    Route::get('/', [AccesoriesController::class, 'index'])->name('accesories');
    Route::get('/add', [AccesoriesController::class, 'create'])->name('accesories-add');
    Route::get('/update', [AccesoriesController::class, 'update'])->name('accesories-update');
    Route::get('/{id}', [AccesoriesController::class, 'show'])->name('accesories-show');
});
Route::prefix('components')->group(function(){

    Route::get('/', [ComponentsController::class, 'index'])->name('components');
    Route::get('/add', [ComponentsController::class, 'create'])->name('components-add');
    Route::get('/update', [ComponentsController::class, 'update'])->name('components-update');
    Route::get('/{id}', [ComponentsController::class, 'show'])->name('components-show');
});
