<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pcgamer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'range',
        'motherboard',
        'processor',
        'ram',
        'stronge',
        'case',
        'powersuplly',
        'case'
    ];
}
