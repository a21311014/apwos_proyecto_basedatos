<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laptopgamer extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'range',
        'model',
        'processor',
        'ram',
        'stronge',
        'battery',
        'screen'
    ];
}
