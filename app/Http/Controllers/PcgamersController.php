<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pcgamer;
class PcgamersController extends Controller
{
    public function index(){

        $pcgamers = Pcgamer::all();

        return view('pcgamers.index', compact('pcgamers'));
    }
    public function create(){
        return view('pcgamers.create');
    }
    public function update(){
        return view('pcgamers.update');
    }
    public function show($id){
        $pcgamer= Pcgamer::findOrFail($id);
        return view('pcgamers.show',compact('pcgamer'));
    }
}
