<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
class BrandsController extends Controller
{
    public function index(){

        $brands = Brand::all();

        return view('brands.index', compact('brands'));
    }
    public function create(){
        return view('brands.create');
    }
    public function update(){
        return view('brands.update');
    }
    public function show($id){
        $brand= Brand::findOrFail($id);
        return view('brands.show',compact('brand'));
    }
}
