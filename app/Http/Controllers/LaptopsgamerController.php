<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Laptopgamer;
class LaptopsgamerController extends Controller
{
    public function index(){

        $laptopsgamer = Laptopgamer::all();

        return view('laptopsgamer.index', compact('laptopsgamer'));
    }
    public function create(){
        return view('laptopsgamer.create');
    }
    public function update(){
        return view('laptopsgamer.update');
    }
    public function show($id){
        $laptopgamer= Laptopgamer::findOrFail($id);
        return view('laptopsgamer.show',compact('laptopgamer'));
    }
}