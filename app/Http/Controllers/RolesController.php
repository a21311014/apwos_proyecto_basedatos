<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
class RolesController extends Controller
{
    public function index(){

        $roles = Role::all();

        return view('roles.index', compact('roles'));
    }
    public function create(){
        return view('roles.create');
    }
    public function update(){
        return view('roles.update');
    }
    public function show($id){
        $role= Role::findOrFail($id);
        return view('roles.show',compact('role'));
    }
}
