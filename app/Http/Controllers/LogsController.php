<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;
class LogsController extends Controller
{
    public function index(){

        $logs = Log::all();

        return view('logs.index', compact('logs'));
    }
    public function create(){
        return view('logs.create');
    }
    public function update(){
        return view('logs.update');
    }
    public function show($id){
        $log= Log::findOrFail($id);
        return view('logs.show',compact('log'));
    }
}