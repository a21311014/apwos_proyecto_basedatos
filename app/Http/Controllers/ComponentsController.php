<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Component;
class ComponentsController extends Controller
{
    public function index(){

        $components = Component::all();

        return view('components.index', compact('components'));
    }
    public function create(){
        return view('components.create');
    }
    public function update(){
        return view('components.update');
    }
    public function show($id){
        $component= Component::findOrFail($id);
        return view('components.show',compact('component'));
    }
}
