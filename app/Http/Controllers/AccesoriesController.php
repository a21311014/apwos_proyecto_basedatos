<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Accesorie;
class AccesoriesController extends Controller
{
    public function index(){

        $accesories = Accesorie::all();

        return view('accesories.index', compact('accesories'));
    }
    public function create(){
        return view('accesories.create');
    }
    public function update(){
        return view('accesories.update');
    }
    public function show($id){
        $accesorie= Accesorie::findOrFail($id);
        return view('accesories.show',compact('accesorie'));
    }
}
