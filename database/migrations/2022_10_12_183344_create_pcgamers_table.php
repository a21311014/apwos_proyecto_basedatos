<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pcgamers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('range');
            $table->string('motherboard');
            $table->string('processor');
            $table->integer('ram');
            $table->integer('storage');
            $table->string('powersupply');
            $table->string('case');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pcgamers');
    }
};
