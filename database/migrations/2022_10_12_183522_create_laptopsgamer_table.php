<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laptopsgamer', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('brands_id');
            $table->string('range');
            $table->string('model');
            $table->integer('processor');
            $table->integer('ram');
            $table->string('storange');
            $table->string('battery');
            $table->string('screen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laptopsgamer');
    }
};
