<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAccesoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accesories', function (Blueprint $table) {
            $table->string('name');
            $table->integer('brands_id');
            $table->string('range');
            $table->string('edition');
        });
        Schema::table('accesories', function (Blueprint $table) {
            $table->dropColumn('mouse');
            $table->dropColumn('mousepads');
            $table->dropColumn('headsets');
            $table->dropColumn('speakers');
            $table->dropColumn('microphones');
            $table->dropColumn('adapters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accesories', function (Blueprint $table) {
            $table->string('mouse');
            $table->string('mousepads');
            $table->integer('headsets');
            $table->integer('speakers');
            $table->string('microphones');
            $table->string('adapters');
        });
        Schema::table('accesories', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('brands_id');
            $table->dropColumn('range');
            $table->dropColumn('edition');
        });
    }
}
