<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('components', function (Blueprint $table) {
            $table->string('name');
            $table->string('brands_id');
            $table->integer('range');
            $table->integer('edition');
        });
        Schema::table('components', function (Blueprint $table) {
            $table->dropColumn('motherboard');
            $table->dropColumn('processor');
            $table->dropColumn('ram');
            $table->dropColumn('storage');
            $table->dropColumn('powersupply');
            $table->dropColumn('case');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('components', function (Blueprint $table) {
            $table->string('motherboard');
            $table->string('proccesor');
            $table->integer('ram');
            $table->integer('edition');
            $table->string('powersupply');
            $table->string('case');
        });
        Schema::table('components', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('brands_id');
            $table->dropColumn('range');
            $table->dropColumn('edition');
        });
    }
}
