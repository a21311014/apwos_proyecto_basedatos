<html>

<head>
    <title>AT9 Soluciones Tecnologicas</title>
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">AT9 Soluciones Tecnologicas</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Components</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Accesories</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Pcgamers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">laptopsgamer</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row text-center">
        <h1 class="letrab">CONOCENOS</h1>
        <div class="col-lg-12 col-sm-12"><img src=></div>
    </div>
    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner" id="slider">
            <div class="carousel-item active">
                <img src="images/FONDO_4.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/FONDO2.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/FONDO3.png" class="d-block w-100" alt="...">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <div class="row text-center">
        <h1 class="navc" class="letrab">Range Hight</h1>
        <div class="col-md-6">
            <img class="PCR" src="images/PCRACE.jpg" alt="">
        </div>
        <div class="col-md-6">
            <br>
            <h1 class="letrab">Las computadoras de alta gama se destacan por
                tener procesadores más rápidos, pantallas
                con resolución alta y mayor rendimiento.
                Ideales para los fanaticos del Gaiming
                brindando todas las comodidades y el alto rendimiento
                para aquellos fanaticos o jugadores de eSports</h1>
        </div>
    </div>
    </div>
    <div class="row text-center">
        <h1 class="navc" class="letrab">Range Medium.</h1>
        <div class="col-md-6">
            <h2 class="letrab">Las computadoras de alta gama se destacan por
                tener procesadores más rápidos, pantallas
                con resolución alta y mayor rendimiento.
                Ideales para los fanaticos del Gaiming
                brindando todas las comodidades y el alto rendimiento
                para aquellos fanaticos o jugadores de eSports</h2>
        </div>
        <div class="col-md-6">
            <img class="PCR" src="images/PC3.jpg" alt="">
        </div>
        <div class="row text-center">
            <h1 class="navc" class="letrab">Laptopsgamer</h1>
            <div class="col-md-12">
                <img class="PCR" src="images/laprog2.png" alt="">
            </div>
            <div class="col-md-12">
                <br>
                <h2 class="letrab">Las computadoras de alta gama se destacan por
                    tener procesadores más rápidos, pantallas
                    con resolución alta y mayor rendimiento.
                    Ideales para los fanaticos del Gaiming
                    brindando todas las comodidades y el alto rendimiento
                    para aquellos fanaticos o jugadores de eSports</h2>
            </div>
            <div class="row text-center">
                <h1 class="navc" class="letrab">COMPONENTES</h1>
                <div class="col-md-4 p-5">
                    <img class="img-fluid" class="PCR" src="images/DISP1.jpg" alt="">
                </div>
                <div class="col-md-4 p-5">
                    <img class="img-fluid" class="PCR" src="images/DISP1.jpg" alt="">
                </div>
                <div class="col-md-4 p-5">
                    <img class="img-fluid" class="PCR" src="images/DISP1.jpg" alt="">
                </div>
                <div class="col-md-4 p-5">
                    <img class="img-fluid" class="PCR" src="images/DISP1.jpg" alt="">
                </div>
                <div class="col-md-4 p-5">
                    <img class="img-fluid" class="PCR" src="images/DISP1.jpg" alt="">
                </div>
                <div class="col-md-4 p-5">
                    <img class="img-fluid" class="PCR" src="images/DISP1.jpg" alt="">
                </div>
                <div class="row text-center">
                    <h1 class="navc" class="letrab">MARCAS</h1>
                    <div class="col-md-4">
                        <img class="PCR" src="images/ROG logo.png" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="PCR" src="images/ROG logo.png" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="PCR" src="images/ROG logo.png" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="PCR" src="images/ROG logo.png" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="PCR" src="images/ROG logo.png" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="PCR" src="images/ROG logo.png" alt="">
                    </div>
                    <div class="container-fluid">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-4 col-sm-12"></div>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="text-center">
                                                <H4 class="letrab"> BUZÓN DE SUGERENCIAS/OPINIONES </H4>
                                            </div>
                                            <div class="12" text-center>
                                                <div class="row">
                                                    <form>
                                                        <div class="col-sm-12 my-1"><input placeholder="Nombres" class="form-control" /></div>
                                                        <div class="col-sm-12"><input placeholder="Apellidos" class="form-control" /></div>
                                                        <div class="row my-1">
                                                            <div class="col-lg-4 col-sm-12"><input placeholder="PAÍS/CD" class="form-control my-1" /></div>
                                                            <div class="col-lg-4 col-sm-12"><input type="phone" placeholder="+52" class="form-control my-1" /></div>
                                                            <div class="col-lg-4 col-sm-12"><input type="email" placeholder="@gmail.com" class="form-control my-1" /></div>
                                                        </div>
                                                        <div class="col-sm-12"></div><input placeholder="Texto" class="form-control" />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="btn btn-danger mt-2 my-2"> ENVIAR </button>
                                </div>
                            </div>

                        </div>

                        <row>
                            <a href="{{route('accesories')}}" class="btn-btn-primary">accesories</a>
                            <a href="{{route('brands')}}" class="btn-btn-primary">brands</a>
                            <!-- <a href="{{route('components')}}" class="btn-btn-primary">components</a> -->
                            <!-- <a href="{{route('laptopsgamer')}}" class="btn-btn-primary">laptopsgamer</a> -->
                            <a href="{{route('pcgamers')}}" class="btn-btn-primary">pcgamers</a>
                            <a href="{{route('logs')}}" class="btn-btn-primary">Logs</a>
                            <a href="{{route('roles')}}" class="btn-btn-primary">Roles</a>
                            <!-- <a href="{{route('users')}}" class="btn-btn-primary">Users</a> -->
                        </row>
                        <script src="bootstrap.js"></script>
</body>

</html>