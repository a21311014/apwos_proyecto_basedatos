@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Dashboard')}}</div>

                <div class="card-body">
                    <h1>Laptopsgamer</h1>
                    @foreach($laptopsgamer as $laptopgamer)
                    {{$laptopgamer->name}} <a href="{{route('laptopsgamer-show', $laptopgamer->id)}}">ver</a>
                    <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection