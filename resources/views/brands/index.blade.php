@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Dashboard')}}</div>

                <div class="card-body">
                    <h1>Brands</h1>
                    <a href="{{ route('brands-add') }}" class="btn btn-danger">Add Brands</a>
                    <br>
                    @foreach($brands as $brand)
                    {{$brand->name}} <a href="{{route('brands-show', $brand->id)}}">ver</a>
                    <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection