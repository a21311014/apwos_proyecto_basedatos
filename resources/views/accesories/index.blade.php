@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Dashboard')}}</div>

                <div class="card-body">
                    <h1>Accesories</h1>
                    <a href="{{ route('accesories-add') }}" class="btn btn-danger">Add Accesories</a>
                    <br>
                    @foreach($accesories as $accesorie)
                    {{$accesorie->name}} <a href="{{route('accesories-show', $accesorie->id)}}">ver</a>
                    <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection