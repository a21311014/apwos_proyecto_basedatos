@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard')}}</div>

                <div class="card-body">
                    <h1>Components Show</h1>
                    id: {{$components->id}}    
                    Name: {{$components->name}} 
                    range: {{$components->range}}    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
