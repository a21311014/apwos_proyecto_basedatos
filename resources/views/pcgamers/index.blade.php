@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Dashboard')}}</div>
                <div class="card-body">
                    <h1>Pcgamers</h1>
                    <a href="{{ route('pcgamers-add') }}" class="btn btn-danger">Add Pcgamers</a>
                    <br>
                    @foreach($pcgamers as $pcgamer)
                    {{$pcgamer->name}} <a href="{{route('pcgamers-show', $pcgamer->id)}}">ver</a>
                    <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
