@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard')}}</div>

                <div class="card-body">
                    <h1>pcgamer Create</h1>
                    <form action="{{ route('brands-add')}}" method="POST">
                        @csrf
                        <label>Name</label>
                        <input type="text" name="name" class="form-control">
                        <label>Range</label>
                        <input type="text" name="range" class="form-control">
                        <label>Motherboard</label>
                        <input type="text" name="range" class="form-control">
                        <label>Proccesor</label>
                        <input type="text" name="range" class="form-control">
                        <label>Ram</label>
                        <input type="text" name="range" class="form-control">
                        <label>Storage</label>
                        <input type="text" name="range" class="form-control">
                        <label>Powersupply</label>
                        <input type="text" name="range" class="form-control">
                        <label>Case</label>
                        <input type="text" name="range" class="form-control">
                        
                </div>
                <div class="card-footer">
                    <button class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection