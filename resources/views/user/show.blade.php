@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard')}}</div>

                <div class="card-body">
                    <h1>User Show</h1>
                    id: {{$users->id}}    
                    Name: {{$users->name}} 
                    range: {{$users->range}}    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection